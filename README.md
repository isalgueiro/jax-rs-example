# JAX-RS examples

Client and server examples using JAX-RS

## Build setup

See 895905763d823ae1a2842e827c5c206a19d176a4.

### Dependencies

- JAX-RS API
- A Jersey (JAX-RS reference implementation) container to run our application
- A JSON parser to support JSON messages 

### Executable jar file

Add a `fatJar` task to generate an executable jar package with all the needed dependencies.

## Service implementation

- Define a simple POJO for returning something in JSON format.
- Define a _service_ class with a method that returns a new instance of that POJO

Service implementation added in 34c2d901b6c079bbb15ba7949e8fdaceb47838be.

## Server main method

Server main method needs to start and setup the Jersey container. Note that `HttpServer#start()` method doesn't block, so we need to keep the main thread open.

Server main method added in 6da5ace039ad54cc61865bd1732a68988227c1cd.

## Client

Client code example in `ItemServiceTest` JUnit test, where we can also check Jersey container usage for unit testing.

Client tests added in 6da5ace039ad54cc61865bd1732a68988227c1cd.
