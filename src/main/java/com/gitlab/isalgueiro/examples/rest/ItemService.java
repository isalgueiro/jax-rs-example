package com.gitlab.isalgueiro.examples.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/item")
public class ItemService {

    @Path("/{name}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Item getItem(@PathParam("name")  String name) {
        System.out.println(name);
        final Item i = new Item();
        i.setColor("Black");
        i.setName(name);
        return i;
    }
}
