package com.gitlab.isalgueiro.examples.rest;

import java.io.Serializable;

public class Item implements Serializable {

    private String name;
    private String color;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(final String color) {
        this.color = color;
    }
}
