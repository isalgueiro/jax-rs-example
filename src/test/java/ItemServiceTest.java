import com.gitlab.isalgueiro.examples.rest.Item;
import com.gitlab.isalgueiro.examples.rest.ItemService;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URI;

public class ItemServiceTest {

    private static HttpServer server;

    @BeforeClass
    public static void startServer() {
        System.out.println("Starting test server");
        final ResourceConfig resourceConfig = new ResourceConfig(ItemService.class);
        resourceConfig.register(JacksonFeature.class);
        server = GrizzlyHttpServerFactory.createHttpServer(
                URI.create("http://0.0.0.0:8888"), resourceConfig);
        try {
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void stopServer() {
        System.out.println("Shutting down test server");
        server.shutdown();
    }

    @Test
    public void testItemService() {
        final ClientConfig configuration = new ClientConfig();
        final Client client = ClientBuilder.newClient(configuration).register(JacksonFeature.class);
        final Item item = client.target(URI.create("http://localhost:8888/item/test"))
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(Item.class);
        System.out.println("test returned " + item.getName());
    }
}
